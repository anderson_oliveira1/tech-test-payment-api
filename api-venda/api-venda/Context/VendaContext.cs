﻿using api_venda.Models;
using Microsoft.EntityFrameworkCore;

namespace api_venda.Context
{
    public class VendaContext : DbContext
    {
        public VendaContext(DbContextOptions<VendaContext> options): base(options)
        {

        }

        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<ProdutoVendido> ProdutoVendidos { get; set; }
    }
}
