﻿namespace api_venda.Models
{
    // classe para representar os produtos da venda
    public class ProdutoVendido
    {
        public int Id { get; set; }
        public int VendaId { get; set; }
        public int? ProdutoId { get; set; }
        public string? Nome { get; set; }
        public decimal? Preco { get; set; }

    }
}
