﻿//namespace api_venda.Models
//{
//    public enum StatusVenda
//    {
//        AguardandoPagamento, PagamentoAprovado, EnviadoParaTransportadora, Entregue, Cancelado
//    }
//}
using System.ComponentModel;


namespace api_venda.Models
{
    public enum StatusVenda
    {
        [Description("Aguardando Pagamento")]
        AguardandoPagamento,
        [Description("Pagamento Aprovado")]
        PagamentoAprovado,
        [Description("Enviado para transportadora")]
        EnviadoParaTransportadora,
        [Description("Entregue")]
        Entregue,
        [Description("Cancelado")]
        Cancelado
    }
}