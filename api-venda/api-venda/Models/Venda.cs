﻿namespace api_venda.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public int VendedorId { get; set; }
        public DateTime? Data { get; set; }
        public decimal? ValorTotal { get; set; }
        public StatusVenda? StatusDaVenda { get; set; }
        public IList<ProdutoVendido> ProdutosVendidos { get; set; }
        public Vendedor? Vendedor { get; set; }

        public Venda()
        {
            ProdutosVendidos = new List<ProdutoVendido>();
            StatusDaVenda = StatusVenda.AguardandoPagamento;
            Data = DateTime.Now;
        }
    }
}
