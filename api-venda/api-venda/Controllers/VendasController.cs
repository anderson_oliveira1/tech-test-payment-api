﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using api_venda.Context;
using api_venda.Models;

namespace api_venda.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendasController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendasController(VendaContext context)
        {
            _context = context;
        }

        // GET: api/Vendas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Venda>>> GetVendas()
        {
            
            // Esse codigo inclui os itens da venda
            return await _context.Vendas.Include(x => x.ProdutosVendidos).ToListAsync();
        }
                
        private bool VendaExists(int id)
        {
            return _context.Vendas.Any(e => e.Id == id);
        }

        // busca uma venda
        [HttpGet("BuscarVenda/{id}")]
        public async Task<IActionResult> BuscarVenda(int id)
        {           

            var venda = await _context.Vendas
                .Include(v => v.ProdutosVendidos)
                .Include(v => v.Vendedor)
                .Where(v => v.Id == id)                
                .FirstOrDefaultAsync();         

            if (venda != null)
            {
                return Ok(venda);
            }
            else
            {
                return NotFound();
            }

        }

        // Método para atualizar o status da venda
        [HttpPost("AtualizarStatus/")]
        public async Task<IActionResult> AtualizarStatusDaVenda(StatusVenda novoStatusDaVenda, int idVenda)
        {
            // procura venda
            var venda = await _context.Vendas.FindAsync(idVenda);

            // confere se a venda existe, se existe muda o status dela e salva a mesma no banco
            if (venda != null)
            {

                if (venda.StatusDaVenda == StatusVenda.AguardandoPagamento && (novoStatusDaVenda == StatusVenda.PagamentoAprovado || novoStatusDaVenda == StatusVenda.Cancelado))
                {
                    venda.StatusDaVenda = novoStatusDaVenda;
                }
                else if (venda.StatusDaVenda == StatusVenda.PagamentoAprovado && (novoStatusDaVenda == StatusVenda.EnviadoParaTransportadora || novoStatusDaVenda == StatusVenda.Cancelado))
                {
                    venda.StatusDaVenda = novoStatusDaVenda;
                }
                else if (venda.StatusDaVenda == StatusVenda.EnviadoParaTransportadora && novoStatusDaVenda == StatusVenda.Entregue)
                {
                    venda.StatusDaVenda = novoStatusDaVenda;
                }

                _context.Vendas.Update(venda);
                await _context.SaveChangesAsync();
                return Ok(venda);
            }
            else
            {
                return NotFound();
            }
        }


    }
}
