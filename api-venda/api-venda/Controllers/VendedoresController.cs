﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using api_venda.Context;
using api_venda.Models;
//using api_venda.Models.Operacao;

namespace api_venda.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendedoresController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendedoresController(VendaContext context)
        {
            _context = context;
        }

        // GET: api/Vendedores
        [HttpGet("ListarVendedores")]
        public async Task<ActionResult<IEnumerable<Vendedor>>> GetVendedores()
        {
            return await _context.Vendedores.ToListAsync();
        }

        // GET: api/Vendedores/5
        [HttpGet("ConsultarVendedor/{id}")]
        public async Task<ActionResult<Vendedor>> GetVendedor(int id)
        {
            var vendedor = await _context.Vendedores.FindAsync(id);

            if (vendedor == null)
            {
                return NotFound();
            }

            return vendedor;
        }

        // PUT: api/Vendedores/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("AtualizarVendedor/{id}")]
        public async Task<IActionResult> PutVendedor(int id, Vendedor vendedor)
        {
            if (id != vendedor.Id)
            {
                return BadRequest();
            }

            _context.Entry(vendedor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VendedorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Vendedores
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("CadastrarVendedor")]
        public async Task<ActionResult<Vendedor>> PostVendedor(Vendedor vendedor)
        {
            _context.Vendedores.Add(vendedor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVendedor", new { id = vendedor.Id }, vendedor);
        }

        // DELETE: api/Vendedores/5
        [HttpDelete("DeletarVendedor/{id}")]
        public async Task<IActionResult> DeleteVendedor(int id)
        {
            var vendedor = await _context.Vendedores.FindAsync(id);
            if (vendedor == null)
            {
                return NotFound();
            }

            _context.Vendedores.Remove(vendedor);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool VendedorExists(int id)
        {
            return _context.Vendedores.Any(e => e.Id == id);
        }

        // Método para fazer venda
        [HttpPost("FazerVenda/")]
        public async Task<IActionResult> FazerVenda(int idVendedor, IList<Produto> produtosDaVenda)
        {

            if (produtosDaVenda.Count >= 1)
            {
                // cria venda, atribui ao vendedor
                Venda novaVenda = new Venda();
                novaVenda.VendedorId = idVendedor;

                // adiciona cada produto da venda como produto vendido
                foreach (var produto in produtosDaVenda)
                {
                    var produtoVendido = new ProdutoVendido();
                    produtoVendido.ProdutoId = produto.Id;
                    produtoVendido.Nome = produto.Nome;
                    produtoVendido.Preco = produto.Preco;

                    novaVenda.ProdutosVendidos.Add(produtoVendido);
                }

                _context.Vendas.Add(novaVenda);
                await _context.SaveChangesAsync();

                return Ok();
            }
            else
            {
                return BadRequest("Venda não concluída. A venda não possui 1 item ao menos.");
            }

        }
    }
}
